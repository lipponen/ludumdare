// win32_ludumdare.cpp

// notes(tommi):
// this is my entry for ludum dare #37
// began 2016-12-11 0859
// here we go

#define _CRT_SECURE_NO_WARNINGS

#include <math.h>
#include <stdio.h>
#include <intrin.h>
#include <process.h>
#include <Windows.h>
#include <Windowsx.h>

#define Assert(cond) if (!(cond)) {*(int*)0=0;}
#define ArrayCount(array) (sizeof(array)/sizeof(array[0]))
#define KiloBytes(size) ((size)*1024U)
#define Clamp(x, m, n) (x < m ? m : x > n ? n : x)

static bool global_running = true;

struct Rect
{
  int x, y;
  int w, h;
};

inline int MathMin(int min, int max) { return min < max ? min : max; }
inline int MathMax(int min, int max) { return min > max ? min : max; }
inline float MathMinf(float min, float max) { return min < max ? min : max; }
inline float MathMaxf(float min, float max) { return min > max ? min : max; }
inline float MathSinf(float r) { return sinf(r); }
inline float MathCosf(float r) { return cosf(r); }
inline float MathSqrtf(float value) { return sqrtf(value); }
inline float MathAbsf(float value) { return fabsf(value); } 
inline float MathSignf(float value) { return value < 0.0f ? -1.0f : 1.0f; }

static int MathRandom()
{
  static int s = (intptr_t)&global_running;
  s = s * 1103515245 + 12345;

  return (unsigned)(s / 65536) % 32768;
}

static float MathRandomf()
{
  float result = MathRandom() / 32768.0f;
  
  return result;
}

static float MathRandomRangef(float min, float max)
{
  return min + (max - min) * MathRandomf();
}

struct Vec2
{
  float x, y;
};

float Vec2Length(Vec2 right)
{
  return MathSqrtf(right.x * right.x + right.y * right.y);
}

Vec2 Vec2Normalize(Vec2 right)
{
  Vec2 result = right;

  float len = Vec2Length(right);
  if (len > 0.0f)
  {
    result.x /= len;
    result.y /= len;
  }

  return result;
}

Vec2 operator+(const Vec2& left, const Vec2& right)
{
  Vec2 result = {};

  result.x = left.x + right.x;
  result.y = left.y + right.y;

  return result;
}

Vec2 operator-(const Vec2& left, const Vec2& right)
{
  Vec2 result = {};

  result.x = left.x - right.x;
  result.y = left.y - right.y;

  return result;
}

Vec2 operator*(const Vec2& left, const float right)
{
  Vec2 result = {};

  result.x = left.x * right;
  result.y = left.y * right;

  return result; 
}

enum KeyCode
{
  KEY_LEFT,
  KEY_RIGHT,
  KEY_UP,
  KEY_DOWN,
  KEY_SPACE,
  KEY_ESCAPE,
  KEY_CODE_COUNT,
};

#include "ludumdare_asset.h"
#include "ludumdare_blit.h"

#include "ludumdare.cpp"

static unsigned Win32FileSize(const char* filename)
{
  WIN32_FILE_ATTRIBUTE_DATA result = {};

  GetFileAttributesExA(filename, GetFileExInfoStandard, &result);

  return result.nFileSizeLow;
}

#include "ludumdare_asset_compiler.cpp"
#include "ludumdare_blit.cpp"

static __int64 Win32GetPerformanceCounter()
{
  LARGE_INTEGER result = {};

  QueryPerformanceCounter(&result);

  return result.QuadPart;
}

static __int64 Win32GetPerformanceFrequency()
{
  LARGE_INTEGER result = {};

  QueryPerformanceFrequency(&result);

  return result.QuadPart; 
}

static double Win32CalculateFrameTime(__int64 start, __int64 end, __int64 freq)
{
  double result = ((double)(end - start) / (double)freq);

  return result > 0.2 ? 0.2 : result;
}

static Bitmap Win32CreateBitmap(int width, int height)
{
  Bitmap result = {};

  result.width = width;
  result.height = height;
  result.bitmap = (unsigned*)_aligned_malloc(width * height * sizeof(unsigned), 16);
  memset(result.bitmap, 0, width * height * sizeof(unsigned));

  return result;
}

struct Win32BackBuffer
{
  int width;
  int height;
  HDC device;
  BITMAPINFO info;
};

static void Win32BlitBitmapToBackBuffer(Bitmap* blit_bitmap, Win32BackBuffer* back_buffer)
{
  int result = StretchDIBits(
    back_buffer->device,
    0, 0, back_buffer->width, back_buffer->height,
    0, 0, blit_bitmap->width, blit_bitmap->height,
    blit_bitmap->bitmap,
    &back_buffer->info,
    DIB_RGB_COLORS,
    SRCCOPY);
  Assert(result != GDI_ERROR);
}

struct Win32AssetLoaderData
{
  const char* filename;
  unsigned filesize;
  Assets* assets;
};

static void Win32AssetBackgroundThreadProc(void* userdata)
{
  Win32AssetLoaderData* data = (Win32AssetLoaderData*)userdata;
  const char* filename = data->filename;
  const unsigned size = data->filesize;
  Assets* assets = data->assets;

  FILE* fin = fopen(filename, "rb");
  Assert(fin);
  fread(assets, 1, size, fin);
  fclose(fin);

  _InterlockedIncrement(&assets->is_valid);

  free(userdata);
}

static Assets* Win32LoadAssetsFromFilename(const char* filename)
{
  Assets* result = 0;

  unsigned size = Win32FileSize(filename);
  result = (Assets*)_aligned_malloc(size, 16);
  memset(result, 0, size);

  Win32AssetLoaderData* data = (Win32AssetLoaderData*)malloc(sizeof(Win32AssetLoaderData));
  data->filename = filename;
  data->filesize = size;
  data->assets = result;

  _beginthread(Win32AssetBackgroundThreadProc, 0, data); 

  return result;
}

static bool Win32AreAssetsLoaded(Assets* assets)
{
  return _InterlockedCompareExchange(&assets->is_valid, 1, 1) == 1 ? true : false;
}

static bool Win32LoadingScreenBlit(Bitmap* target, float timer)
{
#define next() (s/65536) % 32768; s = s * 1103515245 + 12345;
  static int s = 1;

  static const unsigned fill_color = 0xff000000;
  static const unsigned colors[] = 
  {
    0xff111111,
    0xff222222,
    0xff333333,
    0xff444444,
    0xff555555,
    0xff666666,
    0xff777777,
    0xff888888,
    0xff999999,
    0xffaaaaaa,
    0xffbbbbbb,
    0xffcccccc,
    0xffdddddd,
    0xffeeeeee,
    0xffffffff,
  };
  static const unsigned num_colors = ArrayCount(colors);
  static const int bar_width = 2;
  static const int num_bars = 160 / bar_width;

  static float timer_limit = 0.1f;
  static float fade_timer = 0.0f;
  static unsigned fade_color = 0x00ffffff;

  if (timer > timer_limit)
  {
    fade_timer = timer - timer_limit;
    if (fade_timer > 1.0f)
      return true;

    unsigned gray = fade_timer * 256.0f;
    fade_color  = (gray << 24);
  }

  BlitClear(target, fill_color);

  Rect rect = { 16, 16, bar_width, 160 };
  for(unsigned i = 0; i < num_bars; i++) 
  {
    int index = next();
    unsigned color = colors[index % num_colors];
    rect.x = 16 + i * bar_width;
    BlitRectFill(target, rect, color);
  }
#undef next

  Rect dst = { 0, 0, 192, 192 };
  BlitRectFill(target, dst, fade_color, true);

  return false;
}

static KeyCode Win32ConvertToKeyCode(WPARAM wParam)
{
  switch(wParam)
  {
    case VK_LEFT: { return KEY_LEFT; }; break;
    case VK_RIGHT: { return KEY_RIGHT; }; break;
    case VK_UP: { return KEY_UP; }; break;
    case VK_DOWN: { return KEY_DOWN; }; break;
    case VK_SPACE: { return KEY_SPACE; }; break;
    case VK_ESCAPE: { return KEY_ESCAPE; }; break;
  }

  return KEY_CODE_COUNT;
}

static LRESULT CALLBACK Win32MainWindowProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam) 
{
  const int diff_x = 784 - 768;
  const int diff_y = 807 - 768;

  switch(uMsg)
  {
    case WM_WINDOWPOSCHANGED:
    {
      Win32BackBuffer* back_buffer = (Win32BackBuffer*)GetWindowLongPtr(hWnd, GWLP_USERDATA);
      if (back_buffer)
      {
        WINDOWPOS window_pos = *(LPWINDOWPOS)lParam;
        back_buffer->width = window_pos.cx - diff_x;
        back_buffer->height = window_pos.cy - diff_y;
        //back_buffer->width = p.x;
        //back_buffer->height = p.y;
      }
    } break;

    case WM_GETMINMAXINFO:
    {
      LPMINMAXINFO lpMMI = (LPMINMAXINFO)lParam;
      lpMMI->ptMinTrackSize.x = 784;// + diff_x;
      lpMMI->ptMinTrackSize.y = 807;// + diff_y;
      lpMMI->ptMaxTrackSize.x = 1.5f * 784;// + diff_x;
      lpMMI->ptMaxTrackSize.y = 1.5f * 807;// + diff_y;
    } break;

    case WM_CLOSE:
    {
      global_running = false;
      PostQuitMessage(0);
    } break;

    default: 
    {
      return DefWindowProcA(hWnd, uMsg, wParam, lParam); 
    } break;
  }
  return 0;
}

int CALLBACK WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nShowCode)
{
#if 0 //!RELEASE_MODE
  CompileAssets("assets");
#endif

  const int width = 768, height = 768;
  const char* windows_title = "LudumDare#37-\"One Room\"";

  WNDCLASSA windowclass = {};
  windowclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
  windowclass.lpfnWndProc = Win32MainWindowProc;
  windowclass.hInstance = hInstance;
  windowclass.hCursor = LoadCursor(0, IDC_ARROW);
  windowclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
  windowclass.lpszClassName = "ludumdareWindowClass";
  if (!RegisterClassA(&windowclass))
  {
    Assert(false);
    return -1;
  }

  DWORD style = (WS_OVERLAPPEDWINDOW);// & ~(WS_THICKFRAME | WS_MAXIMIZEBOX | WS_MINIMIZEBOX));
  RECT rc = { 0 };
  rc.right = width;
  rc.bottom = height;
  AdjustWindowRect(&rc, style, FALSE);

  int window_width = (rc.right - rc.left);
  int window_height = (rc.bottom - rc.top);

  RECT deskrect;
  HWND desktop = GetDesktopWindow();
  GetWindowRect(desktop, &deskrect);

  int window_center_x = (deskrect.right - window_width) / 2;
  int window_center_y = (deskrect.bottom - window_height) / 2;

  HWND window = CreateWindowExA(
    0,
    windowclass.lpszClassName,
    windows_title,
    style,
    window_center_x,
    window_center_y,
    window_width,
    window_height,
    0,
    0,
    hInstance,
    0);
  if (!window) 
  {
    Assert(false);
    return -1;
  }

  ShowWindow(window, SW_SHOWNORMAL);
  HDC device = GetDC(window);

  MINMAXINFO mmi = { 0 };
  SendMessageA(window, WM_GETMINMAXINFO, 0, (LPARAM)&mmi);

  const __int64 frequency = Win32GetPerformanceFrequency();
  const float frame_target_time = 1.0f / 30.0f;
  double frame_time = frame_target_time;
  float frame_accumulator = 0.0f;
  float total_time = 0.0f;

  int screen_width = width / 4;
  int screen_height = height / 4;
  Bitmap blit_bitmap = Win32CreateBitmap(screen_width, screen_height);

  Win32BackBuffer back_buffer = {};
  back_buffer.width = width;
  back_buffer.height = height;
  back_buffer.device = device;
  back_buffer.info.bmiHeader.biSize = sizeof(back_buffer.info.bmiHeader);
  back_buffer.info.bmiHeader.biWidth = screen_width;
  back_buffer.info.bmiHeader.biHeight = -screen_height;
  back_buffer.info.bmiHeader.biPlanes = 1;
  back_buffer.info.bmiHeader.biBitCount = 32;
  back_buffer.info.bmiHeader.biCompression = BI_RGB;
  SetWindowLongPtr(window, GWLP_USERDATA, (LONG_PTR)&back_buffer);

  Assets* assets = Win32LoadAssetsFromFilename("assets");
  Game* game = (Game*)_aligned_malloc(sizeof(Game), 16);
  memset(game, 0, sizeof(Game));
  game->dt = frame_target_time;
  game->assets = assets;

  // test
  bool initialized = false;
  bool ready_for_play = false;
  float ready_timer = 0.0f;

  while(global_running)
  {
    __int64 frame_start = Win32GetPerformanceCounter();

    MSG msg = {};
    while(PeekMessage(&msg, 0, 0, 0, PM_REMOVE))
    {
      switch(msg.message)
      {
        case WM_KEYDOWN:
        case WM_KEYUP:
        {
          KeyCode code = Win32ConvertToKeyCode(msg.wParam);
          if (code != KEY_CODE_COUNT)
          {
            game->keys[code] = msg.message == WM_KEYDOWN ? true : false;
          }
        } break;

        case WM_CLOSE:
        {
          global_running = false;
        } break;

        default:
        {
          TranslateMessage(&msg);
          DispatchMessage(&msg);
        } break;
      }
    }

    if (!initialized && Win32AreAssetsLoaded(assets))
    {
      initialized = InitializeGameResources(game);
    }

    frame_accumulator += (float)frame_time;
    while (frame_accumulator > frame_target_time)
    {
      frame_accumulator -= frame_target_time;
      total_time += frame_target_time;

      __int64 blit_start = Win32GetPerformanceCounter();
      
      if (initialized && ready_for_play)
      {
        BlitClear(&blit_bitmap, 0xff202020);
        if (GameUpdateAndRender(game, &blit_bitmap))
        {
          global_running = false;
        }
      }
      else 
      {
        ready_timer += frame_target_time;
        if (!initialized) { ready_timer = 0.0f; }
        ready_for_play = Win32LoadingScreenBlit(&blit_bitmap, ready_timer);
      }

      Win32BlitBitmapToBackBuffer(&blit_bitmap, &back_buffer);

      __int64 blit_end = Win32GetPerformanceCounter();
      float blit_time = Win32CalculateFrameTime(blit_start, blit_end, frequency);

      char message[128] = {};
      sprintf(message, "%s [%3.5fms]", windows_title, (blit_time * 1000.0f));
      SetWindowTextA(window, message);

      // reset escape key for pressed once
      game->keys[KEY_ESCAPE] = false;
    }

    Sleep(1);

    __int64 frame_end = Win32GetPerformanceCounter();
    frame_time = Win32CalculateFrameTime(frame_start, frame_end, frequency);
  }

  return 0;
}
