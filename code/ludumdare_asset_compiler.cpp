// ludumdare_asset_compiler.cpp

#define STB_IMAGE_IMPLEMENTATION
#include "dependencies/stb_image.h"

static unsigned CompileImage(const char* filename, FILE* fout)
{
  int x = 0, y = 0, c = 0;
  unsigned char* data = (unsigned char*)stbi_load(filename, &x, &y, &c, 4);
  Assert(data);

  const unsigned size = x * y * 4;

  Image header = {};
  header.width = x;  
  header.height = y;
  fwrite(&header, 1, sizeof(header), fout);
  fwrite(data, 1, size, fout);
  fflush(fout);

  stbi_image_free(data);

  return sizeof(header) + size;
}

#pragma warning(push)
#pragma warning(disable:4456) // declaration of hides previous local declaration
#pragma warning(disable:4457) // declaration of hides function parameter
#pragma warning(disable:4245) // conversion from to, signed/unsigned mismatch
#pragma warning(disable:4701) // potentially uninitialized local variable used
#include "dependencies/stb_vorbis.h"
#pragma warning(pop)

static unsigned CompileAudio(const char* filename, FILE* fout)
{
  short* data = 0;
  int c = 0, s = 0;
  int result = stb_vorbis_decode_filename(filename, &c, &s, &data);
  Assert(result > 0);

  const unsigned size = result * sizeof(short);

  Audio header = {};
  header.num_channels = c;
  header.num_samples = result;

  fwrite(&header, 1, sizeof(header), fout);
  fwrite(data, 1, size, fout);
  fflush(fout);

  free(data);

  return sizeof(header) + size;
}

static void CompileAssets(const char* filename)
{
  struct
  {
    const char* filename;
    AssetType type;
    unsigned id;
  } entries[] =
  {
    // glyphs
    { "../game/c/question.png", ASSET_TYPE_IMAGE, ASSET_ID_CHARACTER_QUESTION_MARK, },
    { "../game/c/a.png", ASSET_TYPE_IMAGE, ASSET_ID_CHARACTER_A, },
    { "../game/c/b.png", ASSET_TYPE_IMAGE, ASSET_ID_CHARACTER_B, },
    { "../game/c/c.png", ASSET_TYPE_IMAGE, ASSET_ID_CHARACTER_C, },
    { "../game/c/d.png", ASSET_TYPE_IMAGE, ASSET_ID_CHARACTER_D, },
    { "../game/c/e.png", ASSET_TYPE_IMAGE, ASSET_ID_CHARACTER_E, },
    { "../game/c/f.png", ASSET_TYPE_IMAGE, ASSET_ID_CHARACTER_F, },
    { "../game/c/g.png", ASSET_TYPE_IMAGE, ASSET_ID_CHARACTER_G, },
    { "../game/c/h.png", ASSET_TYPE_IMAGE, ASSET_ID_CHARACTER_H, },
    { "../game/c/i.png", ASSET_TYPE_IMAGE, ASSET_ID_CHARACTER_I, },
    { "../game/c/j.png", ASSET_TYPE_IMAGE, ASSET_ID_CHARACTER_J, },
    { "../game/c/k.png", ASSET_TYPE_IMAGE, ASSET_ID_CHARACTER_K, },
    { "../game/c/l.png", ASSET_TYPE_IMAGE, ASSET_ID_CHARACTER_L, },
    { "../game/c/m.png", ASSET_TYPE_IMAGE, ASSET_ID_CHARACTER_M, },
    { "../game/c/n.png", ASSET_TYPE_IMAGE, ASSET_ID_CHARACTER_N, },
    { "../game/c/o.png", ASSET_TYPE_IMAGE, ASSET_ID_CHARACTER_O, },
    { "../game/c/p.png", ASSET_TYPE_IMAGE, ASSET_ID_CHARACTER_P, },
    { "../game/c/q.png", ASSET_TYPE_IMAGE, ASSET_ID_CHARACTER_Q, },
    { "../game/c/r.png", ASSET_TYPE_IMAGE, ASSET_ID_CHARACTER_R, },
    { "../game/c/s.png", ASSET_TYPE_IMAGE, ASSET_ID_CHARACTER_S, },
    { "../game/c/t.png", ASSET_TYPE_IMAGE, ASSET_ID_CHARACTER_T, },
    { "../game/c/u.png", ASSET_TYPE_IMAGE, ASSET_ID_CHARACTER_U, },
    { "../game/c/v.png", ASSET_TYPE_IMAGE, ASSET_ID_CHARACTER_V, },
    { "../game/c/w.png", ASSET_TYPE_IMAGE, ASSET_ID_CHARACTER_W, },
    { "../game/c/x.png", ASSET_TYPE_IMAGE, ASSET_ID_CHARACTER_X, },
    { "../game/c/y.png", ASSET_TYPE_IMAGE, ASSET_ID_CHARACTER_Y, },
    { "../game/c/z.png", ASSET_TYPE_IMAGE, ASSET_ID_CHARACTER_Z, },
    { "../game/c/0.png", ASSET_TYPE_IMAGE, ASSET_ID_CHARACTER_0, },
    { "../game/c/1.png", ASSET_TYPE_IMAGE, ASSET_ID_CHARACTER_1, },
    { "../game/c/2.png", ASSET_TYPE_IMAGE, ASSET_ID_CHARACTER_2, },
    { "../game/c/3.png", ASSET_TYPE_IMAGE, ASSET_ID_CHARACTER_3, },
    { "../game/c/4.png", ASSET_TYPE_IMAGE, ASSET_ID_CHARACTER_4, },
    { "../game/c/5.png", ASSET_TYPE_IMAGE, ASSET_ID_CHARACTER_5, },
    { "../game/c/6.png", ASSET_TYPE_IMAGE, ASSET_ID_CHARACTER_6, },
    { "../game/c/7.png", ASSET_TYPE_IMAGE, ASSET_ID_CHARACTER_7, },
    { "../game/c/8.png", ASSET_TYPE_IMAGE, ASSET_ID_CHARACTER_8, },
    { "../game/c/9.png", ASSET_TYPE_IMAGE, ASSET_ID_CHARACTER_9, },
    { "../game/c/dot.png", ASSET_TYPE_IMAGE, ASSET_ID_CHARACTER_DOT, },
    { "../game/c/comma.png", ASSET_TYPE_IMAGE, ASSET_ID_CHARACTER_COMMA, },
    { "../game/c/exclamation.png", ASSET_TYPE_IMAGE, ASSET_ID_CHARACTER_EXCLAMATION_MARK, },
    { "../game/c/hashtag.png", ASSET_TYPE_IMAGE, ASSET_ID_CHARACTER_HASHTAG, },

    // sprites
    { "../game/checkers.png", ASSET_TYPE_IMAGE, ASSET_ID_CHECKERS },
    { "../game/wall.png", ASSET_TYPE_IMAGE, ASSET_ID_WALL },
    { "../game/bullet.png", ASSET_TYPE_IMAGE, ASSET_ID_BULLET },
    { "../game/obsticle_up.png", ASSET_TYPE_IMAGE, ASSET_ID_OBSTICLE_UP },
    { "../game/obsticle_down.png", ASSET_TYPE_IMAGE, ASSET_ID_OBSTICLE_DOWN },
    { "../game/player.png", ASSET_TYPE_IMAGE, ASSET_ID_PLAYER },
    { "../game/trophy.png", ASSET_TYPE_IMAGE, ASSET_ID_TROPHY },

    // sounds
    //{ "../game/pling.ogg", ASSET_TYPE_SOUND, ASSET_ID_PLING },

    // music
    //{ "../game/music.ogg", ASSET_TYPE_MUSIC, ASSET_ID_MUSIC },
  };

  const unsigned num_assets = ArrayCount(entries);

  unsigned offset = sizeof(Assets);
  Asset assets[num_assets] = {};
  
  for(unsigned i = 0; i < num_assets; i++)
  {
    assets[i].state = ASSET_STATE_LOADING;
    assets[i].id = entries[i].id;
    assets[i].type = entries[i].type;
    assets[i].offset = 0;
    assets[i].result = 0;
  }

  Assets header = {};
  FILE* fout = fopen(filename, "wb");
  fwrite(&header, sizeof(header), 1, fout);

  for(unsigned i = 0; i < num_assets; i++)
  {
    assets[i].offset = offset;

    switch(entries[i].type)
    {
      case ASSET_TYPE_IMAGE:
      {
        offset += CompileImage(entries[i].filename, fout);
      } break;
      case ASSET_TYPE_SOUND:
      case ASSET_TYPE_MUSIC:
      {
        offset += CompileAudio(entries[i].filename, fout);
      } break;
    }

    fflush(fout);
  }

  fwrite(assets, 1, sizeof(assets), fout);
  fflush(fout);

  fseek(fout, 0, SEEK_SET);
  header.num_assets = num_assets;
  header.offset = offset;
  fwrite(&header, sizeof(header), 1, fout);

  fclose(fout);
}
