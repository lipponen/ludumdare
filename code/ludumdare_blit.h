// ludumdare_blit.h

static void BlitLine(Bitmap* target, int x0, int y0, int x1, int y1, unsigned color);
static void BlitRectFill(Bitmap* target, Rect dst, unsigned color, bool blend = false);
static void BlitBitmap(Bitmap* target, Bitmap* source, int x, int y);
static void BlitBitmapColor(Bitmap* target, Bitmap* source, int x, int y, unsigned color);
static void BlitText(Bitmap* target, Font* font, int x, int y, unsigned color, const char* text);
