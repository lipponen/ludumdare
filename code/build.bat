@echo off
call "C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\vcvarsall.bat" x64

set ProjectName=ludumdare
set OutputFileName=%ProjectName%_64.exe

set CommonCompilerFlags=-O2 -MT -nologo -fp:fast -fp:except- -Gm- -GR- -EHsc -EHa- -Zo -Oi -FC -Z7 -WX -W4 -wd4100 -wd4189 -wd4201 -wd4244 -wd4459 -wd4505
set CommonCompilerFlags=-DUNICODE=1 -DRELEASE_MODE=1 -DLUDUMDARE_WINDOWS=1 %CommonCompilerFlags%
set CommonLinkerFlagsExe=-incremental:no -opt:ref user32.lib gdi32.lib opengl32.lib dsound.lib dxguid.lib 
set CommonLinkerFlagsLib=-incremental:no -opt:ref -PDB:%ProjectName%_%random%.pdb -EXPORT:UpdateAndRender 

IF NOT EXIST ..\build mkdir ..\build
pushd ..\build

del *.pdb > NUL 2> NUL

@rem cl %CommonCompilerFlags% ..\code\%ProjectName%.cpp -LD /link %CommonLinkerFlagsLib% 
cl %CommonCompilerFlags% ..\code\win32_%ProjectName%.cpp /link %CommonLinkerFlagsExe% /out:%OutputFileName%

popd
