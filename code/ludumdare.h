// ludumdare37.h

static bool IsKeyDown(bool* keys, KeyCode index)
{
  return keys[index];
}

struct Background
{
  Vec2 positions[2];
};

struct Room
{
  int num_walls;
  Vec2 walls[76];
};

struct Obsticle
{
  float speed;
  float min_up, max_up;
  Vec2 collider;
  bool states[8];
  float timers[8];
  Vec2 positions[8];
  Vec2 targets[8];
  int picks[8];
};

struct Player
{
  float timer;
  float safe;
  int score;
  int high;
  Vec2 position;
  Vec2 collider;
};

struct Bullet
{
  float spawn_min;
  float spawn_max;
  float spawner;
  float timer;
  float speed;
  Vec2 min, max;
  Vec2 collider;
  int num_bullets;
  bool actives[128];
  Vec2 positions[128];
  Vec2 directions[128];
};

struct Sprites
{
  Bitmap checkers;
  Bitmap wall;
  Bitmap obsticle_up;
  Bitmap obsticle_down;
  Bitmap bullet;
  Bitmap player;
  Bitmap trophy;
};

enum GameState
{
  GAME_STATE_MENU,
  GAME_STATE_PLAY,
  GAME_STATE_END,
  GAME_STATE_COUNT,
};

struct Game
{
  float dt;
  float time;
  bool keys[KEY_CODE_COUNT];
  GameState state;
  float menu_timer;
  unsigned title_color;
  unsigned end_color;
  unsigned play_color;
  unsigned score_color;
  unsigned high_color;
  Font font;
  Background background;
  Room room;
  Obsticle obsticle;
  Bullet bullet;
  Player player;
  Sprites sprites;
  Assets* assets;
};

