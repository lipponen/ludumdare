// ludumdare.cpp

#include "ludumdare.h"

static bool IsInside(const Vec2& pos, const Vec2& min, const Vec2& max)
{
  if (pos.x < min.x || pos.x > max.x) { return false; }
  if (pos.y < min.y || pos.y > max.y) { return false; }
  return true;
}

static bool Overlap(const Vec2& lmin, const Vec2& lmax, const Vec2& rmin, const Vec2& rmax)
{
  float lhw = (lmax.x - lmin.x) * 0.5f;
  float rhw = (rmax.x - rmin.x) * 0.5f;
  float dhw = (lmin.x + lhw) - (rmin.x + rhw);
  if (MathAbsf(dhw) > MathAbsf(lhw + rhw))
  {
    return false;
  }

  float lhh = (lmax.y - lmin.y) * 0.5f;
  float rhh = (rmax.y - rmin.y) * 0.5f;
  float dhh = (lmin.y + lhh) - (rmin.y + rhh);
  if (MathAbsf(dhh) > MathAbsf(lhh + rhh))
  {
    return false;
  } 

  return true;
}

static void InitializeBackground(Background* background)
{
  background->positions[0] = { -20.0f, -20.0f };
  background->positions[1] = { -20.0f, -20.0f };
}

static void UpdateBackground(Background* background, float time)
{
  float diffx = MathSinf(time) * 10.0f - 20.0f;
  float diffy = MathCosf(time) * 10.0f - 20.0f;
  background->positions[0] = { diffx, diffy };

  diffx = MathSinf(time + 0.2f) * 10.0f - 20.0f;
  diffy = MathCosf(time + 0.3f) * 10.0f - 20.0f;
  background->positions[1] = { diffx, diffy };
}

static void RenderBackground(Bitmap* target, Bitmap* source, Background* background)
{
  BlitBitmap(target, source, background->positions[0].x, background->positions[0].y);
  BlitBitmap(target, source, background->positions[1].x, background->positions[1].y);
}

static void InitializeRoom(Room* room, const float sq)
{
  int index = 0;
  for(int i = 0; i < 20; i++) { room->walls[index++] = { 16.0f, 16.0f + i * sq }; }
  for(int i = 0; i < 20; i++) { room->walls[index++] = { 168.0f, 16.0f + i * sq }; }
  for(int i = 1; i < 19; i++) { room->walls[index++] = { 16.0f + i * sq, 16.0f }; }
  for(int i = 1; i < 19; i++) { room->walls[index++] = { 16.0f + i * sq, 168.0f }; }
  room->num_walls = index;
}

static void UpdateRoom(Room* room, float dt)
{

}

static void RenderRoom(Bitmap* target, Bitmap* source, Room* room)
{
  const int num_walls = room->num_walls;
  for(int i = 0; i < num_walls; i++)
  {
    Vec2 position = room->walls[i];
    BlitBitmap(target, source, position.x, position.y);
  }
}

static void InitializeObsticle(Obsticle* obsticle, Bitmap* bitmap)
{
  obsticle->speed = 16.0f;
  obsticle->min_up = 2.0f;
  obsticle->max_up = 10.0f;
  obsticle->collider = { (float)bitmap->width, (float)bitmap->height };

  const int count = ArrayCount(obsticle->states);
  for (int i = 0; i < count; i++)
  {
    obsticle->states[i] = true;
    obsticle->timers[i] = 0.0f;
    obsticle->positions[i] = { 16.0f, 16.0f };
    obsticle->targets[i] = { 16.0f, 16.0f };
    obsticle->picks[i] = -1;
  }
}

static void ResetObsticle(Obsticle* obsticle)
{
  const Vec2 positions[] = 
  {
    {  24.0f, 24.0f },
    { 160.0f, 24.0f },
    { 160.0f, 160.0f },
    {  24.0f, 160.0f },
  };

  const int count = ArrayCount(obsticle->states);
  for (int i = 0; i < count; i++)
  {
    Vec2 start = positions[MathRandom()%4];
    obsticle->states[i] = true;
    obsticle->timers[i] = 0.0f;
    obsticle->positions[i] = start;
    obsticle->targets[i] = start;
    obsticle->picks[i] = -1;
  }
}

static void SetObsticleTarget(Obsticle* obsticle, int index, Vec2 position)
{
  int rand = MathRandom() % 10;
  if (rand < 2) 
  {
    int player_tile_x = (position.x - 24.0f) / 8.0f;
    int player_tile_y = (position.y - 24.0f) / 8.0f;

    Vec2 tar = { 24.0f + player_tile_x * 8.0f, 24.0f + player_tile_y * 8.0f };
    obsticle->targets[index] = tar;

    return;
  }

  const int count = ArrayCount(obsticle->states);
  int ctx[count] = {};
  int cty[count] = {};

  for (int i = 0; i < count; i++)
  {
    Vec2 tar = obsticle->targets[i];
    ctx[i] = (tar.x - 24.0f) / 8.0f;
    cty[i] = (tar.y - 24.0f) / 8.0f;
  }

  int tile_x = 0;
  int tile_y = 0;

  while(!tile_x && !tile_y)
  {
    tile_x = MathRandom() % 14;
    for(int i = 0; i < count; i++) 
    {
      if (tile_x == ctx[i])
      {
        tile_x = 0;
        break;
      }
    }

    tile_y = MathRandom() % 14;    
    for(int i = 0; i < count; i++) 
    {
      if (tile_y == cty[i])
      {
        tile_y = 0;
        break;
      }
    }
  }

  Vec2 tar = { 40.0f + tile_x * 8.0f, 40.0f + tile_y * 8.0f };

  obsticle->targets[index] = tar;
}

static void UpdateObsticle(Obsticle* obsticle, Vec2 position, float dt)
{
  static const float max_distance = 0.3f;
  const float speed = obsticle->speed * dt;

  const int count = ArrayCount(obsticle->states);
  for (int i = 0; i < count; i++)
  {
    if (!obsticle->states[i])
    {
      Vec2 pos = obsticle->positions[i];
      Vec2 tar = obsticle->targets[i];
      Vec2 diff = tar - pos;
      float dist = Vec2Length(diff);

      if (dist < max_distance)
      {
        obsticle->timers[i] = MathRandomRangef(obsticle->min_up, obsticle->max_up);
        obsticle->states[i] = true;
        obsticle->positions[i] = obsticle->targets[i];
        obsticle->picks[i] = -1;
        continue;
      }

      Vec2 dir = Vec2Normalize(diff);
      int pick = obsticle->picks[i];
      if (pick != -1)
      {
        switch (pick)
        {
          case 0: 
          { 
            if (MathAbsf(diff.x) > 0.2f)
            {
              dir.x = MathSignf(dir.x); 
              dir.y = 0.0f;
            }
          } break;
          case 1: 
          { 
            if (MathAbsf(diff.y) > 0.2f)
            {
              dir.x = 0.0f; 
              dir.y = MathSignf(dir.y);
            }
          } break;
        }
      }
      else if (MathAbsf(dir.x) > MathAbsf(dir.y))
      {
        dir.x = MathSignf(dir.x);
        dir.y = 0.0f;
        obsticle->picks[i] = 0;
      }
      else if (MathAbsf(dir.x) < MathAbsf(dir.y))
      {
        dir.x = 0.0f;
        dir.y = MathSignf(dir.y);
        obsticle->picks[i] = 1;
      }
      else 
      {
        obsticle->picks[i] = MathRandom() % 2;
      }

      pos = pos + dir * speed;
      obsticle->positions[i] = pos;
    }
    else 
    {
      obsticle->timers[i] -= dt;
      if (obsticle->timers[i] < 0.0f)
      {
        obsticle->states[i] = false;
        SetObsticleTarget(obsticle, i, position);
      }
    }
  }
}

static void RenderObsticle(Bitmap* target, Bitmap* source0, Bitmap* source1, Obsticle* obsticle)
{
  const int count = ArrayCount(obsticle->positions);
  for(int i = 0; i < count; i++) 
  {
    bool up = obsticle->states[i];
    Vec2 position = obsticle->positions[i];
    BlitBitmap(target, up ? source0 : source1, position.x, position.y);
  }
}

static void InitializeBullet(Bullet* bullet, Bitmap* bitmap)
{
  bullet->spawn_min = 0.1f;
  bullet->spawn_max = 1.0f;
  bullet->spawner = bullet->spawn_max;
  bullet->timer = bullet->spawner;
  bullet->speed = 48.0f;
  bullet->min = { 24.0f, 24.0f };
  bullet->max = { 164.0f, 164.0f };
  bullet->collider = { (float)bitmap->width, (float)bitmap->height };
  bullet->num_bullets = 0;
}

static void ResetBullet(Bullet* bullet)
{
  const int count = ArrayCount(bullet->actives);
  for (int i = 0; i < count; i++)
  {
    bullet->actives[i] = false;
  }

  bullet->spawner = bullet->spawn_max; 
  bullet->num_bullets = 0;
}

static void ResetBulletSpawnTimer(Bullet* bullet)
{
  bullet->spawner = bullet->spawn_max;
}

static void SpawnBullet(Bullet* bullet, Vec2 position)
{
  if (bullet->num_bullets >= ArrayCount(bullet->positions)) { return; }

  int rand = MathRandom() % 10;

  bullet->num_bullets++;
  int index = 0;
  for (int i = 0; i < ArrayCount(bullet->actives); i++)
  {
    if (!bullet->actives[i])
    {
      index = i;
      break;
    }
  }

  const Vec2 min = bullet->min;
  const Vec2 max = bullet->max;

  Vec2 pos = {};
  Vec2 dir = { MathRandomRangef(-1.0f, 1.0f), MathRandomRangef(-1.0f, 1.0f) }; 

  const int wall_index = MathRandom() % 4;
  switch(wall_index)
  {
    case KEY_LEFT: 
    {
      pos.x = min.x;
      pos.y = MathRandomRangef(min.y, max.y);
      dir.x = 1.0f;
    } break;
    case KEY_RIGHT:
    {
      pos.x = max.x;
      pos.y = MathRandomRangef(min.y, max.y);
      dir.x = -1.0f;
    } break;
    case KEY_UP:
    {
      pos.x = MathRandomRangef(min.x, max.x);
      pos.y = min.y;
      dir.y = 1.0f;
    } break;
    case KEY_DOWN:
    {
      pos.x = MathRandomRangef(min.x, max.x);
      pos.y = max.y;
      dir.y = -1.0f;
    } break;
  }

  if (rand < 2)
  {
    dir = position - pos;
  }

  bullet->actives[index] = true;
  bullet->positions[index] = pos;
  bullet->directions[index] = Vec2Normalize(dir);
}

static void UpdateBullet(Bullet* bullet, Vec2 position, float dt)
{
  bullet->timer += dt;
  bullet->spawner *= 0.995f;
  bullet->spawner = MathMaxf(bullet->spawner, bullet->spawn_min);

  const float speed = bullet->speed * dt;
  const Vec2 min = bullet->min;
  const Vec2 max = bullet->max;

  for (int i = 0; i < ArrayCount(bullet->actives); i++)
  {
    if (!bullet->actives[i]) { continue; }

    Vec2 pos = bullet->positions[i];
    Vec2 dir = bullet->directions[i];
    pos = pos + dir * speed;

    if (!IsInside(pos, min, max))
    {
      bullet->num_bullets--;
      bullet->actives[i] = false;
      continue;
    }

    bullet->positions[i] = pos;
  }

  if (bullet->timer > bullet->spawner)
  {
    bullet->timer = 0.0f;
    SpawnBullet(bullet, position);
  }
}

static void RenderBullet(Bitmap* target, Bitmap* source, Bullet* bullet)
{
  const int count = ArrayCount(bullet->actives);
  for (int i = 0; i < count; i++)
  {
    if (!bullet->actives[i]) { continue; }
    
    Vec2 position = bullet->positions[i];
    BlitBitmap(target, source, position.x, position.y);
  }
}

static void InitializePlayer(Player* player, Bitmap* bitmap)
{
  player->timer = 0.0f;
  player->safe = 2.5f;
  player->score = 0;
  player->high = 0;
  player->position = { 192.0f * 0.5f - 4.0f, 192.0f * 0.5f - 4.0f };
  player->collider = { (float)bitmap->width, (float)bitmap->height };
}

static void ResetPlayer(Player* player)
{
  player->timer = 0.0f;
  player->safe = 2.5f;
  player->score = 0;
  player->position = { 192.0f * 0.5f - 4.0f, 192.0f * 0.5f - 4.0f };
}

static void UpdatePlayer(Player* player, bool* keys, float bullet_spawner, float dt)
{
  player->timer += dt;
  player->safe -= dt;
  player->score = (int)(player->timer * (1.0f / bullet_spawner));
  player->high = MathMax(player->high, player->score);

  const float pixels_per_second = 48.0f;
  const float amount = pixels_per_second * dt;

  Vec2 direction = {};

  if (IsKeyDown(keys, KEY_LEFT))
  {
    direction.x -= 1.0f;
  }
  if (IsKeyDown(keys, KEY_RIGHT))
  {
    direction.x += 1.0f;
  }

  if (IsKeyDown(keys, KEY_UP))
  {
    direction.y -= 1.0f;
  }
  if (IsKeyDown(keys, KEY_DOWN))
  {
    direction.y += 1.0f;
  }

  direction = Vec2Normalize(direction);

  Vec2 position = player->position;
  position = position + direction * amount;
  position.x = Clamp(position.x, 24.0f, 160.0f);
  position.y = Clamp(position.y, 24.0f, 160.0f);
  player->position = position;
}

static void RenderPlayer(Bitmap* target, Bitmap* source, Player* player)
{
  if (player->safe > 0.0f)
  {
    int num = player->safe * 10;
    if (num % 4 == 0) { return; }
    BlitBitmap(target, source, player->position.x, player->position.y);
  }
  else 
  {
    BlitBitmap(target, source, player->position.x, player->position.y);
  }
}

static bool CheckBulletObsticleCollision(Bullet* bullet, Obsticle* obsticle)
{
  bool result = false;

  const int num_obsticles = ArrayCount(obsticle->states);
  const int num_bullets = ArrayCount(bullet->actives);

  const Vec2 ocol = obsticle->collider;
  const Vec2 bcol = bullet->collider;

  for (int i = 0; i < num_obsticles; i++)
  {
    if (!obsticle->states[i]) { continue; }

    Vec2 omin = obsticle->positions[i];
    Vec2 omax = obsticle->positions[i] + ocol;

    for (int k = 0; k < num_bullets; k++)
    {
      if (!bullet->actives[k]) { continue; }

      Vec2 bmin = bullet->positions[k];
      Vec2 bmax = bullet->positions[k] + bcol;

      if (Overlap(omin, omax, bmin, bmax))
      {
        bullet->actives[k] = false;
        result = true;
      }
    }
  }

  return result;
}

static bool CheckBulletPlayerCollision(Bullet* bullet, Player* player)
{
  const int num_bullets = ArrayCount(bullet->actives);
  const Vec2 bcol = bullet->collider;
  const Vec2 pcol = player->collider;

  Vec2 error = { 1.0f, 1.0f };
  Vec2 pmin = player->position + error;
  Vec2 pmax = player->position + pcol - error;

  for (int k = 0; k < num_bullets; k++)
  {
    if (!bullet->actives[k]) { continue; }

    Vec2 bmin = bullet->positions[k];
    Vec2 bmax = bullet->positions[k] + bcol;

    if (Overlap(pmin, pmax, bmin, bmax))
    {
      bullet->actives[k] = false;
      return true;
    }
  }

  return false;
}

static bool CheckObsticlePlayerCollision(Obsticle* obsticle, Player* player)
{
  const int num_obsticles = ArrayCount(obsticle->states);
  const Vec2 ocol = obsticle->collider;
  const Vec2 pcol = player->collider;

  Vec2 error = { 1.0f, 1.0f };
  Vec2 pmin = player->position + error;
  Vec2 pmax = player->position + pcol - error;

  for (int i = 0; i < num_obsticles; i++)
  {
    if (!obsticle->states[i]) { continue; }

    Vec2 omin = obsticle->positions[i];
    Vec2 omax = obsticle->positions[i] + ocol;

    if (Overlap(pmin, pmax, omin, omax))
    {
      obsticle->states[i] = false;
      SetObsticleTarget(obsticle, i, player->position);
      return true;
    }
  }

  return false;
}

static bool InitializeGameResources(Game* game)
{
  Assets* assets = game->assets;

  game->state = GAME_STATE_MENU;

  game->title_color = 0xffdd00dd;
  game->end_color = 0xffff0000;
  game->play_color = 0xff00ff00;
  game->score_color = 0xffdddddd;
  game->high_color = 0xffffff00;

  game->font = FontFromAssets(assets);
  game->sprites.checkers = BitmapFromAssetId(assets, ASSET_ID_CHECKERS);
  game->sprites.wall = BitmapFromAssetId(assets, ASSET_ID_WALL);
  game->sprites.bullet = BitmapFromAssetId(assets, ASSET_ID_BULLET);
  game->sprites.obsticle_up = BitmapFromAssetId(assets, ASSET_ID_OBSTICLE_UP);
  game->sprites.obsticle_down = BitmapFromAssetId(assets, ASSET_ID_OBSTICLE_DOWN);
  game->sprites.player = BitmapFromAssetId(assets, ASSET_ID_PLAYER);
  game->sprites.trophy = BitmapFromAssetId(assets, ASSET_ID_TROPHY);

  InitializeBackground(&game->background);
  InitializeRoom(&game->room, game->sprites.wall.width);
  InitializeObsticle(&game->obsticle, &game->sprites.obsticle_up);
  InitializeBullet(&game->bullet, &game->sprites.bullet);
  InitializePlayer(&game->player, &game->sprites.player);

  return true;
}

static bool GameMenuState(Game* game, Bitmap* target)
{
  if (IsKeyDown(game->keys, KEY_ESCAPE))
  {
    return true;
  }

  if (IsKeyDown(game->keys, KEY_SPACE))
  {
    game->state = GAME_STATE_PLAY;
    game->menu_timer = 0.0f;
    ResetObsticle(&game->obsticle);
    ResetBullet(&game->bullet);
    ResetPlayer(&game->player);
  }

  const char* howtoplay_text = "USE ARROW KEYS";
  Font* font = &game->font;
  int text_width = GetStringWidth(font, howtoplay_text);
  int text_x = (target->width - text_width) / 2;
  int text_y = 82;
  BlitText(target, font, text_x, text_y, 0xff888888, howtoplay_text);
  BlitText(target, font, text_x, text_y-1, game->score_color, howtoplay_text);

  game->menu_timer += game->dt;
  int num = game->menu_timer * 10;
  if (num % 8 > 0) 
  { 
    const char* menu_text = "PRESS SPACE TO START";
    text_width = GetStringWidth(font, menu_text);
    text_x = (target->width - text_width) / 2;
    text_y = 112;
    BlitText(target, font, text_x, text_y, 0xffffffff, menu_text);
    BlitText(target, font, text_x, text_y-1, game->play_color, menu_text);
  }

  return false;
}

static bool GamePlayState(Game* game, Bitmap* target)
{
  Sprites* sprites = &game->sprites;

  UpdateObsticle(&game->obsticle, game->player.position, game->dt);
  UpdateBullet(&game->bullet, game->player.position, game->dt);
  UpdatePlayer(&game->player, game->keys, game->bullet.spawner, game->dt);

  if (CheckBulletObsticleCollision(&game->bullet, &game->obsticle))
  {
    // play sound
  }

  bool player_hit = false;
  if (CheckBulletPlayerCollision(&game->bullet, &game->player))
  {
    player_hit = true;
  }
  if (CheckObsticlePlayerCollision(&game->obsticle, &game->player))
  {
    player_hit = true;
  }
  if (player_hit)
  {
    if (game->player.safe < 0.0f)
    {
      game->state = GAME_STATE_END;
      //ResetPlayer(&game->player);
      //ResetBulletSpawnTimer(&game->bullet);
    }
    else
    {
      // play sounds
    }
  }
  
  RenderObsticle(target, &sprites->obsticle_up, &sprites->obsticle_down, &game->obsticle);
  RenderBullet(target, &sprites->bullet, &game->bullet);
  RenderPlayer(target, &sprites->player, &game->player);

  if (IsKeyDown(game->keys, KEY_ESCAPE))
  {
    game->state = GAME_STATE_MENU;
  }

  return false;
}

static bool GameEndState(Game* game, Bitmap* target)
{
  if (IsKeyDown(game->keys, KEY_ESCAPE) || IsKeyDown(game->keys, KEY_SPACE))
  {
    game->state = GAME_STATE_MENU;
    game->menu_timer = 0.0f;
    return true;
  }

  Font* font = &game->font;

  const char* game_over_text = "GAME OVER";
  int text_width = GetStringWidth(font, game_over_text);
  int text_x = (target->width - text_width) / 2;
  int text_y = 72;
  BlitText(target, font, text_x, text_y, 0xffffffff, game_over_text);
  BlitText(target, font, text_x, text_y-1, game->end_color, game_over_text);

  game->menu_timer += game->dt;
  int num = game->menu_timer * 10;

  if (game->player.score == game->player.high)
  {
    if (num % 8 > 0) 
    { 
      const char* highscore_text = "NEW HIGHSCORE";
      text_width = GetStringWidth(font, highscore_text);
      text_x = (target->width - text_width) / 2;
      text_y = text_y + 20;
      BlitText(target, font, text_x, text_y, 0xffffffff, highscore_text);
      BlitText(target, font, text_x, text_y-1, game->play_color, highscore_text);

      char scoretext[32] = {};
      sprintf(scoretext, "%u", game->player.high);
      text_width = GetStringWidth(font, scoretext);
      text_x = (target->width - text_width) / 2;
      text_y = text_y + 14;
      BlitText(target, font, text_x, text_y, 0xff888888, scoretext);
      BlitText(target, font, text_x, text_y-1, game->score_color, scoretext);
    }
    else
    {
      text_y += 34;
    }
  }

  const char* menu_text = "CONTINUE";
  text_width = GetStringWidth(font, menu_text);
  text_x = (target->width - text_width) / 2;
  text_y = text_y + 34;
  BlitText(target, font, text_x, text_y, 0xffaaaa00, menu_text);
  BlitText(target, font, text_x, text_y-1, game->high_color, menu_text);

  return false;
}

static bool GameUpdateAndRender(Game* game, Bitmap* target)
{
  game->time += game->dt;

  Sprites* sprites = &game->sprites;
  UpdateBackground(&game->background, game->time);
  RenderBackground(target, &sprites->checkers, &game->background);
  RenderRoom(target, &sprites->wall, &game->room);

  switch(game->state)
  {
    case GAME_STATE_MENU:
    {
      if (GameMenuState(game, target))
      {
        return true;
      }
    } break;
    case GAME_STATE_PLAY:
    {
      GamePlayState(game, target);
    } break;
    case GAME_STATE_END:
    {
      GameEndState(game, target);
    } break;
  }

  // always render these below
  Font* font = &game->font;
  const char* text = "ONE ROOM";
  int text_width = GetStringWidth(font, text);
  int text_x = ((192 - text_width) / 2) + MathSinf(game->time * 1.0f) * 46;
  BlitText(target, font, text_x, 4, 0xffffffff, text);
  BlitText(target, font, text_x, 3, game->title_color, text);

  char scoretext[32] = {};
  sprintf(scoretext, "SCORE %4u", game->player.score);
  text_width = GetStringWidth(font, scoretext);
  BlitText(target, font, 192 - text_width - 16, 180, 0xff888888, scoretext);
  BlitText(target, font, 192 - text_width - 16, 179, game->score_color, scoretext);


  char highscore_text[16] = {};
  sprintf(highscore_text, "%u", game->player.high);
  text_width = GetStringWidth(font, highscore_text);
  text_x = 28;
  BlitText(target, font, text_x, 180, 0xffaaaa00, highscore_text);
  BlitText(target, font, text_x, 179, game->high_color, highscore_text);
  BlitBitmap(target, &sprites->trophy, 16, 179);

#if 0
  char bullet_spawner_text[32] = {};
  sprintf(bullet_spawner_text, "%f", game->bullet.spawner);
  BlitText(target, font, 2, 180, 0xffff0000, bullet_spawner_text);
#endif

  return false;
}
