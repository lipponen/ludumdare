// ludumdare_asset.h

// note(tommi): font used is proggy square 12 px

enum AssetId
{
  ASSET_ID_CHARACTER_QUESTION_MARK,
  ASSET_ID_CHARACTER_A,
  ASSET_ID_CHARACTER_B,
  ASSET_ID_CHARACTER_C,
  ASSET_ID_CHARACTER_D,
  ASSET_ID_CHARACTER_E,
  ASSET_ID_CHARACTER_F,
  ASSET_ID_CHARACTER_G,
  ASSET_ID_CHARACTER_H,
  ASSET_ID_CHARACTER_I,
  ASSET_ID_CHARACTER_J,
  ASSET_ID_CHARACTER_K,
  ASSET_ID_CHARACTER_L,
  ASSET_ID_CHARACTER_M,
  ASSET_ID_CHARACTER_N,
  ASSET_ID_CHARACTER_O,
  ASSET_ID_CHARACTER_P,
  ASSET_ID_CHARACTER_Q,
  ASSET_ID_CHARACTER_R,
  ASSET_ID_CHARACTER_S,
  ASSET_ID_CHARACTER_T,
  ASSET_ID_CHARACTER_U,
  ASSET_ID_CHARACTER_V,
  ASSET_ID_CHARACTER_W,
  ASSET_ID_CHARACTER_X,
  ASSET_ID_CHARACTER_Y,
  ASSET_ID_CHARACTER_Z,
  ASSET_ID_CHARACTER_0,
  ASSET_ID_CHARACTER_1,
  ASSET_ID_CHARACTER_2,
  ASSET_ID_CHARACTER_3,
  ASSET_ID_CHARACTER_4,
  ASSET_ID_CHARACTER_5,
  ASSET_ID_CHARACTER_6,
  ASSET_ID_CHARACTER_7,
  ASSET_ID_CHARACTER_8,
  ASSET_ID_CHARACTER_9,
  ASSET_ID_CHARACTER_DOT,
  ASSET_ID_CHARACTER_COMMA,
  ASSET_ID_CHARACTER_EXCLAMATION_MARK,
  ASSET_ID_CHARACTER_HASHTAG,
  ASSET_ID_CHARACTER_COUNT,

  ASSET_ID_CHECKERS,
  ASSET_ID_WALL,
  ASSET_ID_OBSTICLE_UP,
  ASSET_ID_OBSTICLE_DOWN,
  ASSET_ID_BULLET,
  ASSET_ID_PLAYER,
  ASSET_ID_TROPHY,

  ASSET_ID_PLING,

  ASSET_ID_MUSIC,
};

enum AssetType
{
  ASSET_TYPE_IMAGE,
  ASSET_TYPE_SOUND,
  ASSET_TYPE_MUSIC,
  ASSET_TYPE_COUNT,
};

enum AssetState
{
  ASSET_STATE_LOADING,
  ASSET_STATE_CREATED,
  ASSET_STATE_COUNT,
};

struct Asset
{
  unsigned state;
  unsigned id;
  unsigned type;
  unsigned offset;
  unsigned __int64 result;
};

struct Assets
{
  unsigned volatile is_valid;
  unsigned num_assets;
  unsigned offset;
};

static Asset* GetAssetFromId(Assets* assets, AssetId id)
{
  const unsigned num_assets = assets->num_assets;
  const unsigned offset = assets->offset;

  Asset* head = (Asset*)((unsigned char*)assets + offset);
  for(unsigned i = 0; i < num_assets; i++)
  {
    Asset* asset = head + i;
    if (asset->id == (unsigned)id)
    {
      return asset;
    }
  }

  Assert(!"This should never happen, hard crash!");

  return 0;
}

static unsigned __int64 GetAssetResultFromId(Assets* assets, AssetId id)
{
  Asset* asset = GetAssetFromId(assets, id);

  return asset->result;
}


static Asset* GetAssetByIndex(Assets* assets, unsigned index)
{
  const unsigned offset = assets->offset;

  Asset* result = (Asset*)((unsigned char*)assets + offset) + index;

  return result;
}

static unsigned char* GetAssetData(Assets* assets, Asset* asset)
{
  unsigned char* result = (unsigned char*)assets + asset->offset;

  return result;
}

static int AreAllAssetsLoaded(Assets* assets)
{
  if (!assets) 
  { 
    return true; 
  }

  for(unsigned i = 0; i < assets->num_assets; i++)
  {
    Asset* asset = GetAssetByIndex(assets, i);
    if (asset->state != ASSET_STATE_CREATED)
    {
      return false;
    }
  }

  return true;
}

struct Bitmap
{
  int width;
  int height;
  unsigned* bitmap;
};

struct Image
{
  unsigned width;
  unsigned height;
};

struct Audio
{
  unsigned num_channels;
  unsigned num_samples;
};

struct Font
{
  int height;
  Bitmap glyphs[ASSET_ID_CHARACTER_COUNT];
};

static Bitmap BitmapFromAssetData(const void* asset_data)
{
  Image* image = (Image*)asset_data;

  Bitmap result = {};

  result.width = image->width;
  result.height = image->height;
  result.bitmap = (unsigned*)((unsigned char*)asset_data + sizeof(Image));

  return result;
}

static Bitmap BitmapFromAssetId(Assets* assets, AssetId id)
{
  Asset* asset = GetAssetFromId(assets, id);
  unsigned char* asset_data = GetAssetData(assets, asset);

  Bitmap result = BitmapFromAssetData(asset_data);

  return result;
}

static Font FontFromAssets(Assets* assets)
{
  Font result = {};

  int height = 0;
  for (int i = 0; i < ASSET_ID_CHARACTER_COUNT; i++)
  {
    result.glyphs[i] = BitmapFromAssetId(assets, (AssetId)i);
    if (height < result.glyphs[i].height)
    {
      height = result.glyphs[i].height;
    }
  }

  result.height = height;

  return result;
}

static void InitFont(Font* font, Assets* assets)
{
  
}

static unsigned GetGlyphIndex(int ch)
{
  const char* valid_characters = "?ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789.,!#";
  for (int i= 0; i < ASSET_ID_CHARACTER_COUNT; i++)
  {
    if (ch == (int)valid_characters[i])
    {
      return i;
    }
  }

  return 0;
}

static unsigned GetStringWidth(Font* font, const char* text)
{
  unsigned result = 0;

  int len = (int)strlen(text);
  for(int i = 0; i < len; i++)
  {
    int ch = (int)text[i];
    if (ch == ' ')
    {
      result += 4;
      continue;
    }
    else if (ch == '\n') 
    {
      result = 0;
      continue;
    }

    unsigned index = GetGlyphIndex(ch);
    Bitmap* g = font->glyphs + index;

    result += g->width + 1;
  }

  return result;
}