// ludumdare_blit.cpp

static unsigned BlendColors(unsigned dst_color, unsigned src_color, bool alpha = false)
{
  unsigned src_a = (src_color >> 24) & 0xff;  
  if (src_a == 0) { return dst_color; }

  unsigned dst_r = (dst_color >> 16) & 0xff;
  unsigned dst_g = (dst_color >> 8) & 0xff;
  unsigned dst_b = (dst_color >> 0) & 0xff;

  unsigned src_r = (src_color >> 16) & 0xff;
  unsigned src_g = (src_color >> 8) & 0xff;
  unsigned src_b = (src_color >> 0) & 0xff;

  unsigned inv_a = 256 - src_a;
  unsigned final_r = (inv_a * dst_r + src_a * src_r) >> 8;
  unsigned final_g = (inv_a * dst_g + src_a * src_g) >> 8;
  unsigned final_b = (inv_a * dst_b + src_a * src_b) >> 8;

  unsigned result = 0xff000000;
  if (alpha)
  {
    result = (src_a << 24);
  }

  result |= (final_r << 0);
  result |= (final_g << 8);
  result |= (final_b << 16);  

  return result; 
}

static unsigned GetPixel(Bitmap* bitmap, int x, int y)
{
  if (x < 0 || y < 0) { return 0; }
  if (x >= bitmap->width || y >= bitmap->height) { return 0; }
  return bitmap->bitmap[x + y * bitmap->width];
}

static void BlitPutPixel(Bitmap* dst_bitmap, int x, int y, unsigned src_color)
{
  if (x < 0 || y < 0) { return; }
  if (x >= dst_bitmap->width || y >= dst_bitmap->height) { return; }

  if (((src_color >> 24) & 0xff) == 0) { return; }

  unsigned offset = x + y * dst_bitmap->width;
  unsigned* dst = dst_bitmap->bitmap + offset;
  *dst = src_color;
}

static void BlitPutPixelBlend(Bitmap* dst_bitmap, int x, int y, unsigned src_color)
{
  if (x < 0 || y < 0) { return; }
  if (x >= dst_bitmap->width || y >= dst_bitmap->height) { return; }

  unsigned src_a = (src_color >> 24) & 0xff;
  if (src_a == 0) { return; }

  unsigned offset = x + y * dst_bitmap->width;
  unsigned* dst = dst_bitmap->bitmap + offset;

  *dst = BlendColors(*dst, src_color);
}

static void BlitClear(Bitmap* dst_bitmap, unsigned color)
{
  unsigned count = dst_bitmap->width * dst_bitmap->height;
  unsigned* dst = dst_bitmap->bitmap;
  for(unsigned i = 0; i < count; i++)
  {
    dst[i] = color;
  }
}

static void BlitLine(Bitmap* target, int x0, int y0, int x1, int y1, unsigned color)
{
  // https://rosettacode.org/wiki/Bitmap/Bresenham%27s_line_algorithm#C
  int dx = abs(x1 - x0), sx = x0 < x1 ? 1 : -1;
  int dy = abs(y1 - y0), sy = y0 < y1 ? 1 : -1; 
  int err = (dx > dy ? dx : -dy) / 2, e2 = 0;
 
  for(;;)
  {
    BlitPutPixel(target, x0, y0, color);
    if (x0 == x1 && y0 == y1) break;
    e2 = err;
    if (e2 >-dx) { err -= dy; x0 += sx; }
    if (e2 < dy) { err += dx; y0 += sy; }
  } 
}

static void BlitRectFill(Bitmap* target, Rect dst, unsigned color, bool blend)
{
  dst.x = Clamp(dst.x, 0, target->width);
  dst.y = Clamp(dst.y, 0, target->height);
  dst.w = Clamp(dst.w, 0, target->width - dst.x);
  dst.h = Clamp(dst.h, 0, target->height - dst.y);

  for(int y = dst.y; y < dst.y + dst.h; y++)
  {
    for(int x = dst.x; x < dst.x + dst.w; x++)
    {
      if (blend)
        BlitPutPixelBlend(target, x, y, color);
      else
        BlitPutPixel(target, x, y, color);
    }
  }
}

static void BlitBitmap(Bitmap* dst_bitmap, Bitmap* src_bitmap, int x, int y)
{
  Rect src = { 0, 0, src_bitmap->width, src_bitmap->height }; 

  for(int py = 0; py < src.h; py++)
  {
    for(int px = 0; px < src.w; px++)
    {
      unsigned src_color = GetPixel(src_bitmap, px, py);
      BlitPutPixelBlend(dst_bitmap, x + px, y + py, src_color);
    }
  }
}

static void BlitBitmapColor(Bitmap* dst_bitmap, Bitmap* src_bitmap, int x, int y, unsigned color)
{
  Rect src = { 0, 0, src_bitmap->width, src_bitmap->height }; 

  for(int py = 0; py < src.h; py++)
  {
    for(int px = 0; px < src.w; px++)
    {
      unsigned src_a = (GetPixel(src_bitmap, px, py) >> 24) & 0xff;
      if (src_a != 0)
      {
        BlitPutPixel(dst_bitmap, x + px, y + py, color);
      }
    }
  }
}

static void BlitText(Bitmap* dst_bitmap, Font* font, int x, int y, unsigned color, const char* text)
{
  int len = (int)strlen(text), ox = x, yadvance = font->height + 2;
  for (int i = 0; i < len; i++)
  {
    int ch = (int)text[i];
    if (ch == ' ')
    {
      x += 4;
      continue;
    }
    else if (ch == '\n') 
    {
      x = ox;
      y += yadvance;
      continue;
    }

    unsigned index = GetGlyphIndex(ch);
    Bitmap* g = font->glyphs + index;
    BlitBitmapColor(dst_bitmap, g, x, y, color);

    x += g->width + 1;
  }
}
